﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _6211_A2_10005351_SortingSearching
{
    class Program
    {
        static void Main(string[] args)
        {
            int loop = 0;
            List<dateTimeIndexes> datetimeIndexes = new List<dateTimeIndexes>();
            do
            {
                try
                {
                    double max = 0, min2 = 0, ave = 0;  //min,max,ave values to be shared between methods

                    Console.WriteLine("\n\t1. Question 1:\t Find 'n' Maximums from imported text document");
                    Console.WriteLine("\n\t2. Question 2:\t Sort array ascending order. Display Arrays, Min, Max and Ave Values");
                    Console.WriteLine("\n\t3. Question 3:\t Correlate Min,Max,Ave indicies to information imported from timeDate.txt document");
                    Console.WriteLine("\n\t4. Question 4:\t Implement BubbleSort Algorhithm then improve it and measure the time difference");
                    Console.WriteLine("\n\t5. Exit Program");
                    int choice = int.Parse(Console.ReadLine()); //user input for switch case
                    
                    switch (choice)
                    {
                        case 1:
                            findMax(importData());                                  //invoke findMax method to find 'n' maximum values, pass in converted list           
                            break;
                        case 2:
                            selectSort(importData(), out max, out min2, out ave);   //invoke selectionSort method to sort Array and find Min Max Ave Values from importData()
                            break;
                        case 3:
                            selectSort(importData(), out max, out min2, out ave);   //invoke slectSort method taking in importData() out max, min2, ave values for linearSearch
                            linearSeach(datetimeIndexes, max, min2, ave);           //inoke linearSearch taking in max,min2,ave from previous method 
                            compareArrays(datetimeIndexes);                         //inoke compareArrays method taking in List of indexes min, max, ave value instatnces
                            break;
                        case 4:
                            BubbleSortTiming.BubbleSort(importData());              //invoke bubbleSort method taking in imoprtData() from BubbleSortTiming Class
                            BubbleSortTiming.improvedBubbleSort(importData());      //invoke improvedBubbleSort method taking in imoprtData() from BubbleSortTiming Class
                            break;
                        case 5:
                            Console.WriteLine("Exit");
                            loop = 1;                                               //break loop
                            break;
                    }
                }
                catch
                {
                    Console.WriteLine("Incorrect Choice, Try again use numbers 1 - 5 only");
                }
            } while (loop != 1);
        }
            
        public static List<double> importData()//importData Method to import List of type double from text file
        {
            List<string> importList = new List<string>();
            importList = File.ReadLines(@"C:\Users\thema\Desktop\Algorithims_A2\Moisture_Data.txt").ToList();   //copy from txt file into string List                     

            List<double> convertList = new List<double>();  //double List to take convereted string List
            for (int i = 0; i < importList.Count; i++)      //for loop to convert string list to double list
            {
                convertList.Add(Convert.ToDouble(importList[i]));
            }
            return convertList; //return List
        }
        public static void findMax(List<double> convertList)  //findMax method find maximum value in the List
        {
            List<double> highstVals = new List<double>();   //use this list to store required number of highest Values

            Console.WriteLine("\nPlease choose a number to display that many of the Highest values in the array, " +
                "\neg: 2 would return the Two highest Values in the List:\n");    //directive for user
            int selection = int.Parse(Console.ReadLine());  //take in user input  

            int listIndex = 0;                              //variable to store Index number to remove from List
            int counter = 0;

            while (counter != selection)                    //While counter not equal to user selection 
            {
                double max = convertList[0];
                for (int i = 0; i < convertList.Count; i++)   //Find largest value in array 
                {
                    if (convertList[i] > max)                 //if item in List is larger than current Max
                    {
                        max = convertList[i];               //Reset max to current List item
                        listIndex = i;                      //store index of item
                    }
                }
                convertList.RemoveAt(listIndex);            //Remove largest value from List
                highstVals.Add(max);                        //Add current Max Value to highstVals List
                counter++;                                  //add 1 to counter
            }
            Console.WriteLine("List of {0} highest Values:\t", selection);
            printArray(highstVals);                         //print List of 'n' selected highest values    
            methodEnd();
        }
        public static void printArray(List<double> printList) //Method to print array
        {
            Console.WriteLine();
            for (int i = 0; i < printList.Count; i++)
            {
                Console.Write(printList[i] + " ");
            }
            Console.WriteLine();
        }
        public static void selectSort(List<double> convertList, out double max, out double min2, out double ave) //SelectionSort Algorhithm to sort array in Ascending order
        {
            Console.WriteLine("Unsorted Array:");
            printArray(convertList); // print unsorted array
            double temp;    //temp variable used to temporarily store the array value to make swap

            for (int i = 0; i < convertList.Count - 1; i++)     //outer loop to iterate over each item of List
            {
                int min = i;                                    //set min value is index of outer loop
                for (int j = i + 1; j < convertList.Count; j++) //inner loop checks condition and makes swaps
                {
                    if (convertList[j] < convertList[min])      //if first item in List > second item in List
                    {
                        min = j;                                //if true, make minimum = j 
                    }
                }
                if (min != 1)                                   //if minimum value not equal to 1, swap current indexs
                {
                    temp = convertList[i];                      //set temp variable as current covertList index for swap
                    convertList[i] = convertList[min];          //set current convertList item as prevous Minimum
                    convertList[min] = temp;                    //set new min for next loop using temp variable
                }
            }
            Console.WriteLine("\nSorted Array:");
            printArray(convertList);                            // print sorted List
            max = convertList[convertList.Count-1]; ;           //set Maximum value of List
            min2 = convertList[0];                              //set Minimum value of List
            ave = Math.Round(convertList.Average(), 2);         //set Average value of List

            /*double total = 0;
            for (int i= 0; i < convertList.Count; i++)
            {                
                total += convertList[i];                **This loop to sum array and give average manually if necesssary**
            }
            Console.WriteLine("Total Values of Array:\t"+Math.Round(total/100,2));*/
            
            Console.WriteLine("\nMax = {0}\t Min = {1}\t Average = {2}", max, min2, ave);
            methodEnd();
        }
        public static void linearSeach(List<dateTimeIndexes> datetimeIndexes, double max, double min2, double ave)  //Using Max,Min,Ave values from selectSort and returning Array of dateTimeIndexes
        {
            Console.WriteLine("\n\tMax: {0}\t Min: {1}\t Average: {1}", max, min2, ave); //print max,min,ave values from selectSort() Method
            List<double> importList = importData(); //import text file as List of doubles

            Console.WriteLine("\tUnsorted Array:");
            printArray(importList); //print importedlist as un-sorted List
            Console.WriteLine();
            for (int i = 0; i < importList.Count; i++)  //iterate over each item of importList
            {
                if (importList[i] == max)                                                       //if item in importList == max
                {
                    datetimeIndexes.Add(new dateTimeIndexes { type = "Maximum", index = i });   //then record the index position and value in List<datetimeIndexes>
                    Console.WriteLine("Max value occurs at Index:\t" + i);
                }
                else if (importList[i] == min2)                                                 //else if item in importList == min
                {
                    datetimeIndexes.Add(new dateTimeIndexes { type = "Minimum", index = i });   //then record the index position and value in List<datetimeIndexes>
                    Console.WriteLine("Min value occurs at Index:\t" + i);
                }
                else if (importList[i] == ave)                                                  //else if item in importList == ave 
                {
                    datetimeIndexes.Add(new dateTimeIndexes { type = "Average", index = i });   //then record the index position and value in List<datetimeIndexes>
                    Console.WriteLine("Ave value occurs at Index:\t" + i);
                }
            }
            Console.WriteLine("\nAny key to continue...\n");
            Console.ReadKey();
        }
        public static void compareArrays(List<dateTimeIndexes> datetimeIndexes) //Method to take in List of dateTimeIndexes from linearSearch and correlate to data from DateTime_Data List
        {
            List<string> dtData = importDateTImeData(); // import the DateTimeData from the text file

            Console.WriteLine("\nValue Type(Max,Min,Ave) and Index correlated to information from DateTime document:\n");
            foreach (var item in datetimeIndexes)   //print the Indexes at which the values occured in the dtData List as well as if they were max,min or ave
            {
                Console.WriteLine("\tValue Type: {0}\t Index: {1}\t Date/Time: {2}\n",item.type,item.index, dtData[item.index]);
            }
            methodEnd();
        }
        public static List<string> importDateTImeData()//importDateTimeData Method to import data from text file into ad to List of type string
        {
            List<string> importList = new List<string>();
            importList = File.ReadLines(@"C:\Users\thema\Desktop\Algorithims_A2\DateTime_Data.txt").ToList();   //copy from txt file into string List                     
            /*foreach (string item in importList)
            {
                Console.Write(" " + item);    //print items in importList
            }*/
            return importList;  //return List
        }
        public static void methodEnd()
        {
            Console.WriteLine("any key to continue...");
            Console.ReadKey();
            Console.Clear();
        }
    }
    public class dateTimeIndexes    //class containing object constructors for List<dateTimeIndexes>
    {
        public string type = "Max"; //Max,Min or Ave types to be entered
        public int index = 0;       //index of the value from imported Array
    }
    public class BubbleSortTiming   //Class comparing BubbleSort Methods
    {
        public static void BubbleSort(List<double> importList)          //standard bubbleSort Algorhithm
        {
            Console.Clear();
            double temp = 0;

            var watch = System.Diagnostics.Stopwatch.StartNew();        //start timer
            for (int j = 0; j < importList.Count; j++)                  //iterate over each item from List
            {
                for (int sort = 0; sort < importList.Count - 1; sort++) //iterate over each item from List -1
                {
                    if (importList[sort] > importList[sort + 1])        //iterate over each item from List -1
                    {                                                   //**Swapping of List indexes takes place here**
                        temp = importList[sort + 1];                    //make temp variable == index 1
                        importList[sort + 1] = importList[sort];        //make item at List index 1 == index 0
                        importList[sort] = temp;                        //make item at List index 0 == index 1(temp variable)
                    }
                }            
            }
            watch.Stop();   //stop timer
            Console.WriteLine("\t\nTime Taken to sort using BubbleSort Alg:\n" + watch.Elapsed); //print time taken to sort List
            Program.printArray(importList); //print sorted list
            Console.WriteLine("any key to continue...\n");
            Console.ReadKey();
        }   
        public static void improvedBubbleSort(List<double> importList)      //improved bubblesort Alg
        {
            double temp;

            var watch = System.Diagnostics.Stopwatch.StartNew();            //start timer
            for (int j = 0; j < importList.Count; j++)                      //iterate over each item from List
            {
                int swaps = 0;                                              //variable to count swaps
                for (int sort = 0; sort < importList.Count - j-1; sort++)   //iterate ver each item from List -1 **added code here to reduce iterations (-j-1) --->improvement 1<---
                {
                    if (importList[sort] > importList[sort + 1])            //iterate ver each item from List -1
                    {                                                       //**Swapping of List indexes takes place here**
                        temp = importList[sort + 1];                        //make temp variable == index 1
                        importList[sort + 1] = importList[sort];            //make item at List index 1 == index 0
                        importList[sort] = temp;                            //make item at List index 0 == index 1(temp variable)
                        swaps++;                                            //swap counter
                    }
                }
                if(swaps == 0)          //condition to monitor swaps --->improvement 2<---
                {
                    break;              //if no swaps occur, data is sorted, terminate
                }
            }
            watch.Stop();   //stop timer

            Console.WriteLine("Time Taken to sort using Improved BubbleSort Alg:\n" + watch.Elapsed); //print time taken to sort List
            Program.printArray(importList); //print sorted list
            Program.methodEnd();
        }
    }
}
